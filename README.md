# xfbg
Wallpaper changer that automatically sets panel color for Xfce.

## Configuration
### Installation
- Install LuaJIT from your distribution's repositories.
  - Debian, Ubuntu, Mint, Elementary and derivatives: `sudo apt-get install luajit`
  - Arch, Antergos, Manjaro: `sudo pacman -S install luajit`
- Copy xfbg to `/bin/`
- Copy sh.lua and xfconf.lua to `/usr/share/lua/5.1/`

### Variety
For use in Variety, copy the file `set_wallpaper` to `"$HOME/.config/variety/scripts/set_wallpaper"`. Overwriting the original file. Make sure to backup the original file, in case you wish to stop using xfbg.

### Intensity
You can change the __intensity__ variable to change the relative brightness of the panel.

## Usage
`xfbg <path-to-wallpaper>`  
Or use Variety with the modified set_wallpaper script.
