local sh = require('sh')
local xfconf = sh.command('xfconf-query')

local xf = {}

function xf.listProperties(channel, match)
	local properties = sh.split(xfconf('-c', channel, '-l'))

	if match then
		local result = {}
		for _, property in pairs(properties) do
			if string.find(property, match) ~= nil then
				table.insert(result, property)
			end
		end

		return result
	else
		return properties
	end
end

function xf.readProperty(channel, property)
	local property = xfconf('-c', channel, '-p', property)

	return property
end

function xf.setProperty(channel, property, ...)
	local args = {...}
	local values = {}
	for i=1, #args do
		table.insert(values, '-s')
		table.insert(values, args[i])
	end

	xfconf('-c', channel, '-p', property, unpack(values))
end

return xf
